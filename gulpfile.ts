import * as gulp from 'gulp';
import * as gulpLoadPlugins from 'gulp-load-plugins';
import * as runSequence from 'run-sequence';
import {DEV_DEST, PROD_DEST, APP_SRC, APP_BASE, APP_DEST, SASS_DEST, DEPENDENCIES} from './tools/config';
import {join} from 'path';
import * as codeChange from './tools/tasks/code_change';
const plugins = <any>gulpLoadPlugins();
const tsProject = plugins.typescript.createProject('tsconfig.json', {
  typescript: require('typescript')
});

gulp.task('html.dev', (e:any) => {
    return gulp.src(join(APP_SRC, 'index.html'))
        .pipe(inject('shims'))
        .pipe(inject('libs'))
        .pipe(inject())
        .pipe(gulp.dest(DEV_DEST));
});

gulp.task('html.prod', () => {
    return gulp.src(join(APP_SRC, 'index.html'))
        .pipe(inject('libs'))
        .pipe(inject())
        .pipe(plugins.htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest(PROD_DEST));
});

gulp.task('sass.dev', () => {
    return gulp.src(join(SASS_DEST, '**/*.scss'))
        .pipe(plugins.sass().on('error', plugins.sass.logError))
        .pipe(plugins.concat('all.css'))
        .pipe(gulp.dest(join(DEV_DEST, 'css')));
});

gulp.task('sass.prod', () => {
    return gulp.src(join(SASS_DEST, '**/*.scss'))
        .pipe(plugins.sass({outputStyle: 'compressed'}).on('error', plugins.sass.logError))
        .pipe(plugins.concat('all.css'))
        .pipe(gulp.dest(join(PROD_DEST, 'css')));
});

gulp.task('clean.dev', () => {
    return gulp.src(DEV_DEST)
        .pipe(plugins.clean());
});

gulp.task('clean.prod', () => {
    return gulp.src(PROD_DEST)
        .pipe(plugins.clean());
})

gulp.task('js.dev', () => {
    return gulp.src(join(APP_SRC + '/app', '**/*.ts'))
      .pipe(gulp.dest(join(DEV_DEST, 'ts')));
});

gulp.task('js.prod', () => {
    return gulp.src(join(APP_SRC, '**/*.ts'))
      .pipe(plugins.sourcemaps.init())
      .pipe(plugins.typescript(tsProject))
      .pipe(plugins.concat('all.js'))
      .pipe(plugins.uglify())
      .pipe(plugins.sourcemaps.write())
      .pipe(gulp.dest(join(PROD_DEST, 'js')));
});

gulp.task('build.dev', (done: any) => runSequence('clean.dev', 'js.dev', 'sass.dev', 'html.dev', done));

gulp.task('build.prod', (done: any) => runSequence('clean.prod', 'js.prod', 'sass.prod', 'html.prod', done));

gulp.task('watch', (e:any) => {
    gulp.watch(join(APP_SRC, '**/*.ts'),(e:any) =>
      runSequence('js.dev', () => codeChange.changed(e.files))
    );
    gulp.watch(join(SASS_DEST, '**/*.scss'),(e:any) =>
      runSequence('sass.dev', () => codeChange.changed(e.files))
    );
    gulp.watch(join(APP_SRC, '**/*.html'),(e:any) =>
      runSequence('html.dev', () => codeChange.changed(e.files))
    );
    codeChange.listen();
});

gulp.task('server.dev', (done: any) => runSequence('build.dev', 'watch', done));

gulp.task('server.prod', ['build.prod']);

function inject(name?: string) {
  return plugins.inject(gulp.src(getInjectablesDependenciesRef(name), { read: false }), {
    name,
    transform: transformPath()
  });
}

function getInjectablesDependenciesRef(name?: string) {
  return DEPENDENCIES
    .filter(dep => dep['inject'] && dep['inject'] === (name || true))
    .map(mapPath);
}

function mapPath(dep: any) {
  return dep.src;
}

function transformPath() {
  return function (filepath: string) {
    arguments[0] = join(APP_BASE, filepath) + `?${Date.now()}`;
    return slash(plugins.inject.transform.apply(plugins.inject.transform, arguments));
  };
}

function slash(str: string) {
	let isExtendedLengthPath = /^\\\\\?\\/.test(str);
	let hasNonAscii = /[^\x00-\x80]+/.test(str);

	if (isExtendedLengthPath || hasNonAscii) {
		return str;
	}

	return str.replace(/\\/g, '/');
}
