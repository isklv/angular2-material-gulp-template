import {PORT, APP_DEST, APP_BASE, DIST_DIR} from '../config';
import * as browserSync from 'browser-sync';

let runServer = () => {
  let baseDir = APP_DEST;
  let routes:any = {
    [`${APP_BASE}${APP_DEST}`]: APP_DEST,
    [`${APP_BASE}node_modules`]: 'node_modules',
  };

  if (APP_BASE !== '/') {
    routes[`${APP_BASE}`] = APP_DEST;
    baseDir = `${DIST_DIR}/empty/`;
  }

  browserSync.init({
    middleware: [require('connect-history-api-fallback')({index: `${APP_BASE}index.html`})],
    port: PORT,
    startPath: APP_BASE,
    server: {
      baseDir: baseDir,
      routes: routes
    }
  });
};

let listen = () => {
  runServer();
};

let changed = (files: any) => {
  if (!(files instanceof Array)) {
    files = [files];
  }
  browserSync.reload(files.path);
};

export { listen, changed };
