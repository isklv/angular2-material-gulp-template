import {InjectableDependency, Environments} from './project.config.interfaces';

export const ENVIRONMENTS: Environments = {
  DEVELOPMENT: 'dev',
  PRODUCTION: 'prod'
};

export class ProjectConfig{

    APP_TITLE            = 'My Angular2 App';

    APP_SRC              = 'src';
    APP_BASE             = '/';
    ASSETS_SRC           = `${this.APP_SRC}/assets`;

    ENV                  = getEnvironmet();

    PORT                 = 3333;

    DIST_DIR             = 'dist';
    DEV_DEST             = `${this.DIST_DIR}/dev`;
    PROD_DEST            = `${this.DIST_DIR}/prod`;
    APP_DEST             = `${this.DIST_DIR}/${this.ENV}`;
    SASS_DEST            = `${this.ASSETS_SRC}/sass`;
    JS_DEST              = `${this.APP_DEST}/js`;

    NPM_DEPENDENCIES: InjectableDependency[] = [
      { src: 'systemjs/dist/system-polyfills.src.js', inject: 'shims', env: ENVIRONMENTS.DEVELOPMENT },
      { src: 'es6-shim/es6-shim.js', inject: 'shims', env: ENVIRONMENTS.DEVELOPMENT },
      { src: 'systemjs/dist/system.src.js', inject: 'shims', env: ENVIRONMENTS.DEVELOPMENT },
      { src: 'angular2/bundles/angular2-polyfills.js', inject: 'shims' },
      { src: 'rxjs/bundles/Rx.js', inject: 'libs', env: ENVIRONMENTS.DEVELOPMENT },
      { src: 'angular2/bundles/angular2.js', inject: 'libs', env: ENVIRONMENTS.DEVELOPMENT },
      { src: 'typescript/lib/typescript.js', inject: 'libs', env: ENVIRONMENTS.DEVELOPMENT },

      { src: '@angular2-material/button/button.js', inject: 'libs' },
      { src: '@angular2-material/button/button.css', inject: true },
      { src: '@angular2-material/card/card.js', inject: 'libs' },
      { src: '@angular2-material/card/card.css', inject: true },
      { src: '@angular2-material/checkbox/checkbox.js', inject: 'libs' },
      { src: '@angular2-material/checkbox/checkbox.css', inject: true },
      { src: '@angular2-material/input/input.js', inject: 'libs' },
      { src: '@angular2-material/input/input.css', inject: true },
      { src: '@angular2-material/list/list.js', inject: 'libs' },
      { src: '@angular2-material/list/list.css', inject: true },
      { src: '@angular2-material/progress-circle/progress-circle.js', inject: 'libs' },
      { src: '@angular2-material/progress-circle/progress-circle.css', inject: true },
      { src: '@angular2-material/radio/radio.js', inject: 'libs' },
      { src: '@angular2-material/radio/radio.css', inject: true },
      { src: '@angular2-material/sidenav/sidenav.js', inject: 'libs' },
      { src: '@angular2-material/sidenav/sidenav.css', inject: true },
      { src: '@angular2-material/toolbar/toolbar.js', inject: 'libs' },
      { src: '@angular2-material/toolbar/toolbar.css', inject: true }
    ];

    APP_ASSETS: InjectableDependency[] = [
      { src: `${this.APP_DEST}/css/all.css`, inject: true, vendor: false },
      //{ src: `${this.APP_DEST}/js/all.js`, inject: true, vendor: false }
    ];

    get DEPENDENCIES():InjectableDependency[]{
      return normalizeDependencies(this.NPM_DEPENDENCIES.filter(filterDependency.bind(null, this.ENV)))
        .concat(this.APP_ASSETS);
    }
}

function getEnvironmet():String{
    return ENVIRONMENTS.DEVELOPMENT;
}

function filterDependency(env: string, d: InjectableDependency): boolean {
  if (!d.env) {
    d.env = Object.keys(ENVIRONMENTS).map(k => ENVIRONMENTS[k]);
  }
  if (!(d.env instanceof Array)) {
    (<any>d).env = [d.env];
  }
  return d.env.indexOf(env) >= 0;
}

export function normalizeDependencies(deps: InjectableDependency[]) {
  deps
    .filter((d:InjectableDependency) => !/\*/.test(d.src)) // Skip globs
    .forEach((d:InjectableDependency) => d.src = require.resolve(d.src));
  return deps;
}
